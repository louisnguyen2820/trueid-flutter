import 'package:flutter/material.dart';
import 'package:trueid/StartScreen.dart';
import 'package:trueid/ResultScreen.dart';
/**
 * 
 * Wan't run this project ?, run "flutter run" at command line
 * Wan't use file assets ?, go to pubspec.yaml and add this file location to assets resource
 * 
 */
void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Trueid product by flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: StartScreen(),
    );
  }
}