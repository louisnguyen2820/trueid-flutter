import 'package:flutter/material.dart';

class ResultScreen extends StatelessWidget {

  String avatarSrc = "https://hinhnendephd.com/wp-content/uploads/2019/10/anh-avatar-dep.jpg";
  String name = "NGUYEN PHUONG ANH";
  String dob = "24/07/1993";
  String gender= "UNKNOWN";
  String id = "1234567890";

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width; // width of screen
    double deviceHeight = MediaQuery.of(context).size.height; // height of screen
    return MaterialApp(
      title: 'trueid app',
      home: Scaffold(
        body: new Stack(
          children: <Widget>[
              new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/background-result.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            Center(
              child: Column(
                  crossAxisAlignment : CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: deviceWidth,
                      height: 250,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft:  const Radius.circular(15.0),
                          bottomRight: const Radius.circular(15.0)
                        ),
                        color: Colors.white,
                      ),
                      child: Center(
                        child: Column(
                          crossAxisAlignment : CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              width: deviceWidth*0.35,
                              height: deviceWidth*0.35,
                              margin: const EdgeInsets.only(top: 25.0),
                              child: CircleAvatar(
                                radius: 100.0,
                                backgroundImage: NetworkImage(this.avatarSrc), // src image result
                              ),
                            ),
                            Container(
                              child: Text(this.name, style: TextStyle(color: Colors.blue, fontSize: 22, fontWeight: FontWeight.w500 )), // name
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Text("Date of birth", style: TextStyle(color: Colors.grey, fontSize: 12)),
                                      Container(
                                        margin: const EdgeInsets.only(top: 10.0),
                                        child: Text(this.dob, style: TextStyle(color: Colors.black, fontSize: 14)), // day of birth
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Text("Gender", style: TextStyle(color: Colors.grey, fontSize: 12)),
                                      Container(
                                        margin: const EdgeInsets.only(top: 10.0),
                                        child: Text(this.gender, style: TextStyle(color: Colors.black, fontSize: 14)), // gender
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Text("ID Number", style: TextStyle(color: Colors.grey, fontSize: 12)),
                                      Container(
                                        margin: const EdgeInsets.only(top: 10.0),
                                        child: Text(this.id, style: TextStyle(color: Colors.black, fontSize: 14)), // id
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            )
                          ],
                        ),
                      )
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 100),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children:[
                          Column(
                            children: [
                              Container(
                                child: Stack(
                                  children: [
                                    Image.asset('assets/images/idvalid-icon.png', width: deviceWidth*0.12, fit: BoxFit.cover,),
                                    Positioned(
                                      right: 0,
                                      child: Image.asset('assets/images/pass-icon.png', width: deviceWidth*0.06, fit: BoxFit.cover,),
                                    )
                                  ],
                                )
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 10.0),
                                child: Text("ID Validation", style: TextStyle(color: Colors.white, fontSize: 13)), // gender
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                child: Stack(
                                  children: [
                                    Image.asset('assets/images/faceverification-icon.png', width: deviceWidth*0.12, fit: BoxFit.cover,),
                                    Positioned(
                                      right: 0,
                                      child: Image.asset('assets/images/error-icon.png', width: deviceWidth*0.06, fit: BoxFit.cover,),
                                    )
                                  ],
                                )
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 10.0),
                                child: Text("Face Verification", style: TextStyle(color: Colors.white, fontSize: 13)), // gender
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                child: Stack(
                                  children: [
                                    Image.asset('assets/images/liveness-icon.png', width: deviceWidth*0.12, fit: BoxFit.cover,),
                                    Positioned(
                                      right: 0,
                                      child: Image.asset('assets/images/pass-icon.png', width: deviceWidth*0.06, fit: BoxFit.cover,),
                                    )
                                  ],
                                )
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 10.0),
                                child: Text("Liveness Detection", style: TextStyle(color: Colors.white, fontSize: 13)), // gender
                              ),
                            ],
                          ),
                        ]
                      )
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 35.0),
                      child: ButtonTheme(
                        minWidth: deviceWidth*0.5,
                        height: 55,
                        child: RaisedButton(
                          onPressed: ()=>{
                            /**
                             * do some thing when click button
                             */
                          },
                          child: Text("Start another demo",  style: TextStyle(color: Colors.white, fontSize: 17),),
                          color: Colors.blue,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ),
            ],
        )
      )
    );
  }
}
