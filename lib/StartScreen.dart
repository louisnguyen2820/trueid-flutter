import 'package:flutter/material.dart';
import 'package:trueid/ResultScreen.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width; // width of screen
    double deviceHeight = MediaQuery.of(context).size.height; // height of screen
    return MaterialApp(
      title: 'trueid app',
      home: Scaffold(
        body: new Stack(
          children: <Widget>[
              new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/background-login.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Center(
                child: Column(
                    crossAxisAlignment : CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 100.0),
                        child: Image.asset('assets/images/logo-trueid.png', width: deviceWidth*0.5, fit: BoxFit.cover,),
                      ),
                      Container(
                        margin: const EdgeInsets.only(bottom: 100.0),
                        child: ButtonTheme(
                          minWidth: deviceWidth*0.5,
                          height: 55,
                          child: RaisedButton(
                            onPressed: ()=>{
                              /**
                               * do some thing when click button
                               */
                              // navigation
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ResultScreen()),
                              )
                            },
                            child: Text("START DEMO",  style: TextStyle(color: Colors.blue, fontSize: 17 ,fontWeight: FontWeight.bold),),
                            color: Colors.white,
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        // margin: const EdgeInsets.fromLTRB(500, 200, 500, 200),
                        child: Text("Copyright © 2019 Datalab", style: TextStyle(color: Colors.white, fontSize: 12 ),)
                      ),
                    ],
                  )
                ),
            ],
        )
      )
    );
  }
}
